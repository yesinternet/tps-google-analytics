<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Google_Analytics
 * @subpackage Tps_Google_Analytics/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Google_Analytics
 * @subpackage Tps_Google_Analytics/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Google_Analytics {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Google_Analytics_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->define_constants();
		$this->load_config();

		$this->plugin_name = 'tps-google-analytics';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Define Constants.
	 */
	private function define_constants() {
		
		$this->define( 'TPS_GOOGLE_ANALYTICS_PLUGIN_PATH', WP_PLUGIN_DIR.'/tps-google-analytics' );
		$this->define( 'TPS_GOOGLE_ANALYTICS_TEMPLATES_PATH', TPS_GOOGLE_ANALYTICS_PLUGIN_PATH.'/public/partials' );
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Load config file
	 */
	private function load_config() {

		if ( is_file( TPS_GOOGLE_ANALYTICS_PLUGIN_PATH . '/config.php' ) )
		
			require_once TPS_GOOGLE_ANALYTICS_PLUGIN_PATH . '/config.php';

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Google_Analytics_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Google_Analytics_i18n. Defines internationalization functionality.
	 * - Tps_Google_Analytics_Admin. Defines all hooks for the admin area.
	 * - Tps_Google_Analytics_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-google-analytics-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-google-analytics-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-google-analytics-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-google-analytics-public.php';

		/**
		 * The class responsible for adding GA tracking code
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-google-analytics-tracking-code.php';

		/**
		 * The class responsible for GA helpers
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-google-analytics-helpers.php';
		
		/**
		* The class responsible for tracking GA events
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-google-analytics-track-event.php';

		$this->loader = new Tps_Google_Analytics_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Google_Analytics_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Google_Analytics_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Google_Analytics_Admin( $this->get_plugin_name(), $this->get_version() );

		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Google_Analytics_Public( $this->get_plugin_name(), $this->get_version() );

		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		
		//Add GA tracking code
		$this->loader->add_action( 'tps-google-analytics', 'Tps_Google_Analytics_Tracking_Code', 'load_template' );

		//Track user registration event
		$this->loader->add_action( 'user_register', 'Tps_Google_Analytics_Track_Event', 'user_register' );	
		
		//Track add/remove to wishlist event: Removed, done via JS instead
		//$this->loader->add_action( 'tps_wishlist_after_add_product', 'Tps_Google_Analytics_Track_Event' , 'wishlist_add_product' , 10 , 4);	
		//$this->loader->add_action( 'tps_wishlist_after_remove_product', 'Tps_Google_Analytics_Track_Event' , 'wishlist_remove_product' , 10 , 4);	
		//$this->loader->add_action( 'tps_wishlist_after_wishlist_public', 'Tps_Google_Analytics_Track_Event' , 'wishlist_public' , 10 , 1);	

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Google_Analytics_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}

<?php

/**
 * The class responsible for adding the GA tracking code
 */

class Tps_Google_Analytics_Tracking_Code {


	/**
	 * Load the template that contains the GA tracking code.
	 */
	public static function load_template() {
		
		include TPS_GOOGLE_ANALYTICS_TEMPLATES_PATH.'/tracking-code.php';

	}



}

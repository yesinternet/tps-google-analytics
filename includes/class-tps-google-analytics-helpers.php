<?php

/**
 * The class responsible for GA helpers
 */

class Tps_Google_Analytics_Helpers {

	/**
	 * Get GA cid from cookie
	 */
	public static function get_cid() {
		
		$_ga_cookie = explode( '.' , $_COOKIE['_ga'] ) ;

		if (count ($_ga_cookie) != 4 ){
			error_log('_ga cookie format not GA1.2.xxxxxxxx.xxxxxxx');
			return false;
		}

		$cid = $_ga_cookie[2] . '.' . $_ga_cookie[3];

		return $cid;

	}

	/*
	* Send a request to the GA measurement protocol
	*/
	public static function mp_request( $payload = array() ) {

		//GA Measurement portocol endpoint
		$host = 'https://www.google-analytics.com';
		$endpoint = '/collect';
		$url = $host.$endpoint;

		//Open connection
		$ch = curl_init();

		//set the url and more options
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, 1 );
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query( $payload ) );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		//Execute post
		curl_exec($ch);

		//Close connection
		curl_close($ch);

		return true;

	}

}
<?php

/**
 * The class responsible for tracking GA events
 */

class Tps_Google_Analytics_Track_Event {

	/**
	 * Track user registration event
	 */
	public static function user_register() {
		
		$cid = Tps_Google_Analytics_Helpers::get_cid();

		if ( !$cid ){
			return;
		}

		$registration_source = 'wordpress';

		if ( isset ( $_SESSION['user_details'] ) )
		{
			$user_details = $_SESSION['user_details'];

			if ( isset ($user_details -> status) &&  $user_details -> status != "SUCCESS" ){
				error_log('Social registration response status is ['. $user_details -> status . '] and different than SUCCESS.');
			}

			if ( isset ($user_details -> status) && $user_details -> status == "SUCCESS" ){

				$registration_source = ( isset( $user_details -> deutype ) ) ? $user_details -> deutype : 'wordpress';
			}

		}

		//Event tracking post variables
		$payload = array(
			
			'v' => urlencode('1'), // Version.
			'tid' => urlencode(TPS_GOOGLE_ANALYTICS_TRACKING_CODE), // Tracking ID / Property ID.
			'cid' => urlencode( $cid ), // Anonymous Client ID.
			
			't' => urlencode('event'), // Event hit type
			'ec' => urlencode('User'), // Event Category (Required)
			'ea' => urlencode('registration'), // Event Action (Required)
			'el' => urlencode( $registration_source ), // Event label (Optional)
			//'ev' => urlencode('Website') // Event value (Optional)
		);

		$mp_request = Tps_Google_Analytics_Helpers::mp_request( $payload );

	}

	/*
	public static function wishlist_add_product  ( $add_product_to_wishlist , $product_id , $user_id , $user_wishlist_product_ids ){

		$cid = Tps_Google_Analytics_Helpers::get_cid();

		if ( !$cid ){
			return;
		}


		//Event tracking post variables
		$payload = array(
			
			'v' => urlencode('1'), // Version.
			'tid' => urlencode(TPS_GOOGLE_ANALYTICS_TRACKING_CODE), // Tracking ID / Property ID.
			'cid' => urlencode( $cid ), // Anonymous Client ID.
			
			't' => urlencode('event'), // Event hit type
			'ec' => urlencode('Wishlist'), // Event Category (Required)
			'ea' => urlencode('addproduct'), // Event Action (Required)
			'el' => urlencode( get_the_title( $product_id ) ), // Event label (Optional)
			//'ev' => urlencode( intval( $product_id ) ) // Event value (Optional)
		);

		$mp_request = Tps_Google_Analytics_Helpers::mp_request( $payload );

	}

	public static function wishlist_remove_product  ( $remove_product_from_wishlist , $product_id , $user_id , $user_wishlist_product_ids ){

		$cid = Tps_Google_Analytics_Helpers::get_cid();

		if ( !$cid ){
			return;
		}


		//Event tracking post variables
		$payload = array(
			
			'v' => urlencode('1'), // Version.
			'tid' => urlencode(TPS_GOOGLE_ANALYTICS_TRACKING_CODE), // Tracking ID / Property ID.
			'cid' => urlencode( $cid ), // Anonymous Client ID.
			
			't' => urlencode('event'), // Event hit type
			'ec' => urlencode('Wishlist'), // Event Category (Required)
			'ea' => urlencode('removeproduct'), // Event Action (Required)
			'el' => urlencode( get_the_title( $product_id ) ), // Event label (Optional)
			//'ev' => urlencode( intval( $product_id ) ) // Event value (Optional)
		);

		$mp_request = Tps_Google_Analytics_Helpers::mp_request( $payload );

	}

	public static function wishlist_public  ( $user_id ){

		$cid = Tps_Google_Analytics_Helpers::get_cid();

		if ( !$cid ){
			return;
		}


		//Event tracking post variables
		$payload = array(
			
			'v' => urlencode('1'), // Version.
			'tid' => urlencode(TPS_GOOGLE_ANALYTICS_TRACKING_CODE), // Tracking ID / Property ID.
			'cid' => urlencode( $cid ), // Anonymous Client ID.
			
			't' => urlencode('event'), // Event hit type
			'ec' => urlencode('Wishlist'), // Event Category (Required)
			'ea' => urlencode('public'), // Event Action (Required)
			//'el' => urlencode( get_the_title( $product_id ) ), // Event label (Optional)
			//'ev' => urlencode( intval( $product_id ) ) // Event value (Optional)
		);

		$mp_request = Tps_Google_Analytics_Helpers::mp_request( $payload );

	}

	*/
}
#Which are the events being tracked by the plugin

* User
	* Category: User
	* Action: registration
	* Label: [wordpress],[facebook]

* Wishlist
	* Category: Wishlist
	* Action: add_product, remove_product, signup, publish
	* Label: [product_name]

* Wishlist
	* Category: Wishlist
	* Action: publish 
	* Label: none

* Forms
	* Category: Forms
	* Action: submit
	* Label: [form_name]

* Currency Switcher
	* Category: Currency
	* Action: click
	* Label: [currency_code]

* Product
	* Category: Product
	* Action: buy_now_button_click
	* Label: [product_name]



#Configuration

##Create the configuration file config.php
Create a config.php file in the plugin root folder, based on config.sample.php and add the GA tracking code
```
define('TPS_GOOGLE_ANALYTICS_TRACKING_CODE', 'UA-XXXXXXXX-X');
```
##How to track Contact form 7 submission via GA Events
To set the event tracking code open the Additional Settings tab in the contact form editor screen, and insert the following line into the field:
```
on_sent_ok: "ga('send', 'event', 'Forms', 'submit' ,'[Contact_Form_Name]');"
```
(function( $ ) {
	'use strict';

	jQuery(document).ready(function($){

		//Wishlist buttons
		$( 'body' ).on('wishlist-ga-event', '.tps-wishlist-btn' , function( event , action , product ){

			ga('send', 'event', 'Wishlist', action , product );

		});

		//Currency selection
		$( 'body' ).on('click', '.tps-currency-select-link' , function( event ){

			ga('send', 'event', 'Currency', 'select' , $(this).data("curcode") );

		});

		//Add to cart button
		$( 'body' ).on('click', '.tps-buy-now-btn' , function( event ){

			var product  = ( $(this).data("product") ) ;

			ga('send', 'event', 'Product', 'buy_now_button_click' , product );
			
		});

		//var owl = $('.owl-carousel.tps_slider_54_1');
		//owl.owlCarousel();
		// Listen to owl events:
		//owl.on('changed.owl.carousel', function(event) {
		   // console.log(event);

		//})

	});

})( jQuery );
